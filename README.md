# ICHLL_Brenon

La version la plus à jour du papier est dispo
[ici](https://perso.liris.cnrs.fr/abrenon/no-backup/ICHLL_Brenon.pdf).

Il peut être généré sur tout système où [guix](https://guix.gnu.org/) est
installé depuis la racine de ce dépôt en lançant la commande:

```
guix shell -m manifest.scm -- make
```
