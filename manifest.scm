(use-modules (guix packages)
             (gnu packages base)
             (gnu packages graphviz)
             (gnu packages haskell-xyz)
             (gnu packages pdf)
             (gnu packages tex))

(let
  ((pandoc-fignos (load "pandoc-fignos.scm")))
  (packages->manifest
    (list gnu-make graphviz pandoc poppler texlive pandoc-fignos)))
