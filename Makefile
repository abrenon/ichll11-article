FIGURES = action_t1 arbre boumerang_t7 cathète_t9 dictionaries europe_t16 gelocus_t18 last_page_top_left_t1 sanjo_t29
SNIPPETS = $(wildcard snippets/*.md)
FILTERS = pandoc-fignos
FILE_NAME = ICHLL_Brenon
DEPEDENCIES = $(FIGURES:%=ressources/%.png) $(SNIPPETS:%.md=%.png) biblio.bib

all: $(FILE_NAME).pdf

ICHLL_Brenon.pdf: $(DEPEDENCIES)
ICHLL_Brenon.docx: $(DEPEDENCIES)

%.pdf: %.md
	LANG=fr_FR.UTF-8 pandoc $< $(FILTERS:%=--filter %) --bibliography=biblio.bib --csl=apa.csl --citeproc -o $@

%.png: %.pdf
	pdftocairo -png -singlefile -r 400 $^ $(basename $@)

%.png: %.gv
	dot -Tpng $< -o $@

%.docx: %.md
	LANG=fr_FR.UTF-8 pandoc $< --to docx --number-sections $(FILTERS:%=--filter %) --bibliography=biblio.bib --csl=apa.csl --citeproc -o $@

