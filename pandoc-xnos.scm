(use-modules ((gnu packages python-xyz) #:select (python-psutil))
             ((gnu packages textutils) #:select (python-pandocfilters))
             (guix build-system python)
             ((guix download) #:select (url-fetch))
             ((guix licenses) #:select (gpl3))
             (guix packages))

(package
  (name "python-pandoc-xnos")
  (version "2.5.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "pandoc-xnos" version))
      (sha256
        (base32 "1bjhwbfz67fm8ckvhhadi7y82cnlv2mnarfwfg2wwklyi667663w"))))
  (build-system python-build-system)
  (arguments
    `(#:tests? #f
      #:phases
      (modify-phases %standard-phases
                     (replace 'check
                              (lambda* (#:key tests? #:allow-other-keys)
                                       (if tests?
                                         (begin
                                           (invoke "ls")
                                           (invoke "pwd")
                                           (invoke "./test/test.py"))))))))
  (propagated-inputs (list python-pandocfilters python-psutil))
  (home-page "https://github.com/tomduck/pandoc-xnos")
  (synopsis "Library code for the pandoc-xnos filter suite.")
  (description "Library code for the pandoc-xnos filter suite.")
  (license gpl3))
