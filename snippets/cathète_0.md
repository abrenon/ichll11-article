---
header-includes:
	\pagestyle{empty}
	\usepackage{graphicx}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
        paperwidth=5.6cm,
        paperheight=0.5cm,
        margin=0cm
	}
---

```xml
<div xml:id="cathète-0"></div>
```
