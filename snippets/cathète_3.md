---
header-includes:
	\pagestyle{empty}
	\usepackage{graphicx}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
        paperwidth=13.8cm,
        paperheight=4.7cm,
        margin=0cm
	}
---

```xml
<div xml:id="cathète-0">
    <lb/><head>CATHÈTE (<abbr>Archit.</abbr>).</head>
	<div type="sense" n="0">
		<p>
			On désigne ainsi la ligne
			<lb/>d’aplomb qui passe par l’œil de la volute du chapiteau
			<lb/>ionique et qui sert, comme point fixe, au tracé de cette
			<lb/>volute.
		</p>
	</div>
</div>
```
