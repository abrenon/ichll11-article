---
header-includes:
	\pagestyle{empty}
	\usepackage{graphicx}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
        paperwidth=6.3cm,
        paperheight=1.8cm,
        margin=0cm
	}
---

```xml
<figure>
	<graphic url="t7p725_1.png"/>
	<figDesc>Boumerangs.</figDesc>
</figure>
```
