---
header-includes:
	\pagestyle{empty}
	\usepackage{graphicx}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
        paperwidth=12.3cm,
        paperheight=3.1cm,
        margin=0cm
	}
---

```xml
<lb/>nerie générale de Madrid et du diocèse de Tolède. Elle est
<pb n="1247" />
<fw type="header">ALCALA-DE-HÉNARÈS</fw>
<fw type="pageNum">— 1200 —</fw>
<lb/>située sur la rive droite du rio Hénarès qui se jette un peu
<lb/>plus loin dans un affluent du Tage, le rio Jarama. Elle por¬
<lb/>tait du temps des Romains le nom de Complutum. Son nom
```
