---
header-includes:
	\pagestyle{empty}
	\usepackage{graphicx}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
        paperwidth=9.2cm,
        paperheight=1.8cm,
        margin=0cm
	}
---

```xml
<p>
	(V. <ref target="#boeuf-0">Boeuf</ref> et
	<ref target="#chevrotain-0">Chevrotain</ref>).
</p>
```
