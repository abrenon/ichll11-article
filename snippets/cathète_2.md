---
header-includes:
	\pagestyle{empty}
	\usepackage{graphicx}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
        paperwidth=8.8cm,
        paperheight=1.8cm,
        margin=0cm
	}
---

```xml
<div xml:id="cathète-0">
    <head>CATHÈTE (<abbr>Archit.</abbr>)</head>
	<div type="sense" n="0"></div>
</div>
```
