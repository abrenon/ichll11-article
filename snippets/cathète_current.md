---
header-includes:
	\pagestyle{empty}
	\usepackage{graphicx}
	\usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	\geometry{
        paperwidth=12.5cm,
        paperheight=2.8cm,
        margin=0cm
	}
---

```xml
<div xml:id="cathète-0">
	<lb/><head>CATHÈTE</head> (Archit.). On désigne ainsi la ligne
	<lb/>d’aplomb qui passe par l’œil de la volute du chapiteau
	<lb/>ionique et qui sert, comme point fixe, au tracé de cette
	<lb/>volute.
</div>
```
